using System;
using System.Collections;
using System.Collections.Generic;
using System.Threading;
using System.Threading.Tasks;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityMultiplayer.Unity.GameOperations;

namespace UnityMultiplayer.Unity.Net
{
  public sealed class NetworkManager : MonoBehaviour
  {
    public const int SERVER_PLAYER_ID = 0;
    internal const int NETWORK_TICKS_PER_SECOND = 1;
    internal const float NETWORK_TICK_INTERVAL = 1f / NETWORK_TICKS_PER_SECOND;
    internal const string PLAYER_PREFAB_NAME = "Player";

    public int PlayerId
    {
      get
      {
        if (!IsInitialized)
          throw new InvalidOperationException();

        return _playerId;
      }
    }

    #if UNITY_SERVER
    public MatchServerBehaviour MatchServerBehaviour { get; private set; }
    #else
    public GameClientBehaviour Client { get; private set; }
    #endif

    public INetworkHandler NetworkHandler { get; private set; }

    public static NetworkManager Instance { get; private set; }

    [SerializeField]
    private List<GameObject> prefabsToRegister;

    private TaskScheduler mainThreadTaskScheduler;

    private void Awake()
    {
      if (Instance && Instance != this)
      {
        Destroy(this);
        return;
      }

      Instance = this;
      DontDestroyOnLoad(this);
      #if UNITY_SERVER
      MatchServerBehaviour = gameObject.AddComponent<MatchServerBehaviour>();
      NetworkHandler = MatchServerBehaviour;
      Init(SERVER_PLAYER_ID);
      #else
      Client = gameObject.AddComponent<GameClientBehaviour>();
      NetworkHandler = Client;
      #endif
      NetworkHandler.NetworkViewManager.RegisterPrefab(PLAYER_PREFAB_NAME, PlayerPrefab);
      foreach (GameObject prefab in prefabsToRegister)
        NetworkHandler.NetworkViewManager.RegisterPrefab(prefab.name, prefab);
      SceneManager.sceneLoaded += SceneManagerOnSceneLoaded;
      mainThreadTaskScheduler = TaskScheduler.FromCurrentSynchronizationContext();
    }

    public bool IsInitialized { get; private set; }

    internal void Init(int playerId)
    {
      if (IsInitialized)
        throw new InvalidOperationException();

      IsInitialized = true;
      _playerId = playerId;
    }

    private void SceneManagerOnSceneLoaded(Scene scene, LoadSceneMode sceneMode)
    {
      if (scene.name != MainScene)
      {
        if (_networkCoroutine != null)
          StopCoroutine(_networkCoroutine);
        return;
      }

      _networkCoroutine = StartCoroutine(NetworkTickCoroutine());
    }

    private Coroutine _networkCoroutine;

    // ReSharper disable ConvertToAutoProperty
    [SerializeField]
    private string mainScene;

    public string MainScene => mainScene;

    [SerializeField]
    private string menuScene;

    public string MenuScene => menuScene;

    [SerializeField]
    private GameObject playerPrefab;

    private int _playerId;

    public GameObject PlayerPrefab => playerPrefab;
    // ReSharper restore ConvertToAutoProperty

    public IEnumerator NetworkTickCoroutine()
    {
      GameUpdatePacket initPacket = NetworkHandler.GetInitializePacket();
      if (initPacket != null)
        NetworkHandler.SendPacket(initPacket);
      while (true)
      {
        yield return new WaitForSecondsRealtime(NETWORK_TICK_INTERVAL);

        NetworkHandler.SendPacket(NetworkHandler.GetNetworkTickPacket());
      }

      // ReSharper disable once IteratorNeverReturns
    }

    public void RunOnMainThread(Action action)
    {
      Task.Factory.StartNew(action, CancellationToken.None, TaskCreationOptions.None, mainThreadTaskScheduler);
    }
  }
}
