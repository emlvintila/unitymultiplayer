using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityMultiplayer.Unity.GameOperations;
using UnityMultiplayer.Unity.Net.NetworkBehaviours;
using Object = UnityEngine.Object;

namespace UnityMultiplayer.Unity.Net
{
  public abstract class AbstractNetworkViewManagerImpl : INetworkViewManager
  {
    private readonly Dictionary<int, NetworkView> _networkViews = new Dictionary<int, NetworkView>();
    private readonly Dictionary<string, GameObject> _registeredPrefabs = new Dictionary<string, GameObject>();
    public IReadOnlyDictionary<string, GameObject> RegisteredPrefabsMap => _registeredPrefabs;
    public IReadOnlyDictionary<int, NetworkView> NetworkViewsMap => _networkViews;

    public void Dispatch(AbstractGameOperation operation)
    {
      switch (operation)
      {
        case GameOperationNetworkInstantiate instance:
          NetworkInstantiate(instance.PrefabName, default, default,
            instance.PlayerId, instance.ViewId, instance.InstanceIds);
          return;
        case GameOperationNetworkDestroy destroy:
          NetworkDestroy(destroy.ViewId);
          return;
        // TODO: Move this to NetworkView
        case GameOperation gameOperation:
          int viewId = gameOperation.ViewId;
          if (!NetworkViewsMap.TryGetValue(viewId, out NetworkView view))
          {
            throw new ArgumentException($"The operation's {nameof(GameOperation.ViewId)}={viewId} is invalid.",
              nameof(gameOperation));
          }

          int instanceId = gameOperation.InstanceId;
          if (!view.NetworkBehavioursMap.TryGetValue(instanceId, out NetworkBehaviour behaviour))
          {
            throw new ArgumentException($"The operation's {nameof(GameOperation.InstanceId)}={instanceId} is invalid.",
              nameof(gameOperation));
          }

          behaviour.HandleOperation(gameOperation);
          return;
      }
    }

    public void DispatchAll(IEnumerable<AbstractGameOperation> gameOperations)
    {
      foreach (AbstractGameOperation operation in gameOperations)
        Dispatch(operation);
    }

    public void RegisterPrefab(string prefabName, GameObject prefab)
    {
      if (!prefab.GetComponent<NetworkView>())
        throw new ArgumentException(nameof(prefab));
      if (RegisteredPrefabsMap.ContainsKey(prefabName))
        throw new ArgumentException("A prefab was already registered with that name.", nameof(prefabName));

      _registeredPrefabs[prefabName] = prefab;
    }

    public GameOperationNetworkInstantiate NetworkInstantiate(string prefabName, Vector3 position, Quaternion rotation,
      int playerId, int viewId, IEnumerable<int> instanceIds)
    {
      if (!RegisteredPrefabsMap.TryGetValue(prefabName, out GameObject prefab))
      {
        throw new ArgumentException($"The prefab \"{prefabName}\" was not registered for instantiation.",
          nameof(prefabName));
      }

      if (NetworkViewsMap.ContainsKey(viewId))
      {
        throw new ArgumentException($"The prefab with {nameof(NetworkView.ViewId)}={viewId} was already instantiated.",
          nameof(viewId));
      }

      GameObject obj = Object.Instantiate(prefab, position, rotation, null);
      NetworkView view = obj.GetComponent<NetworkView>();
      view.InternalPlayerId = playerId;
      view.InternalViewId = viewId;
      _networkViews[viewId] = view;
      NetworkBehaviour[] behaviours = view.GetComponentsInChildren<NetworkBehaviour>(true);
      RegisterBehavioursForView(view, behaviours, instanceIds);
      return new GameOperationNetworkInstantiate(viewId, playerId, prefabName,
        behaviours.Select(b => b.InstanceId));
    }

    public GameOperationNetworkDestroy NetworkDestroy(int viewId)
    {
      if (!NetworkViewsMap.TryGetValue(viewId, out NetworkView view))
      {
        throw new ArgumentException($"Attempted to destroy nonexistent {nameof(NetworkView)} " +
          $"with {nameof(NetworkView.ViewId)}={viewId}", nameof(viewId));
      }

      Object.Destroy(view);
      _networkViews.Remove(viewId);
      return new GameOperationNetworkDestroy(viewId);
    }

    protected abstract void RegisterBehavioursForView(NetworkView view, IEnumerable<NetworkBehaviour> behaviours,
      IEnumerable<int> instanceIds);
  }
}
