using UnityEngine;
using UnityEngine.UI;
using UnityMultiplayer.Shared;
using UnityMultiplayer.Unity.Net;

namespace UI.Menu
{
  public class MatchInfoBehaviour : MonoBehaviour
  {
    #if !UNITY_SERVER
    [SerializeField]
    private Button button;

    private MatchInfo matchInfo;

    [SerializeField]
    private Text text;

    public MatchInfo MatchInfo
    {
      get => matchInfo;
      set
      {
        matchInfo = value;
        text.text = $"RemoteEndPoint: {value.EndPointString}\nMatchId: {value.MatchId}";
      }
    }

    private void Awake()
    {
      button.onClick.AddListener(JoinMatch);
    }

    private void JoinMatch()
    {
      NetworkManager.Instance.Client.GameClient.JoinMatchRoom(MatchInfo.MatchId);
    }
    #endif
  }
}
