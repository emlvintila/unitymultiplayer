using System.Collections.Generic;
using UnityEngine;
using UnityMultiplayer.Unity.GameOperations;

namespace UnityMultiplayer.Unity.Net
{
  public interface INetworkViewManager : INetworkDispatcher
  {
    IReadOnlyDictionary<string, GameObject> RegisteredPrefabsMap { get; }

    IReadOnlyDictionary<int, NetworkView> NetworkViewsMap { get; }

    void RegisterPrefab(string prefabName, GameObject prefab);

    GameOperationNetworkInstantiate NetworkInstantiate(string prefabName,
      Vector3 position, Quaternion rotation, int playerId,
      int viewId, IEnumerable<int> instanceIds);

    GameOperationNetworkDestroy NetworkDestroy(int viewId);
  }
}
