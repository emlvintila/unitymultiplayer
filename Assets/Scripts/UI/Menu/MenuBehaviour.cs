using System.Collections.Generic;
using UnityEngine;

namespace UI.Menu
{
  public class MenuBehaviour : MonoBehaviour
  {
    #if !UNITY_SERVER
    [SerializeField]
    private List<SubMenu> subMenus = new List<SubMenu>();

    private void Awake()
    {
      foreach (SubMenu subMenu in subMenus)
        subMenu.Owner = this;
    }

    public void HideAll()
    {
      foreach (SubMenu subMenu in subMenus)
        subMenu.Hide();
    }
    #endif
  }
}
