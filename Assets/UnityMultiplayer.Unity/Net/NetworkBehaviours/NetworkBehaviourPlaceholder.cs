using UnityEngine;

namespace UnityMultiplayer.Unity.Net.NetworkBehaviours
{
  public class NetworkBehaviourPlaceholder : MonoBehaviour
  {
    [SerializeField]
    private string prefabName;

    // ReSharper disable once ConvertToAutoProperty
    public string PrefabName => prefabName;
  }
}
