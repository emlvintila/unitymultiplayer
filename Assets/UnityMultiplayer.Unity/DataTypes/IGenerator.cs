namespace UnityMultiplayer.Unity.DataTypes
{
  public interface IGenerator<out T>
  {
    T Next();
  }
}
