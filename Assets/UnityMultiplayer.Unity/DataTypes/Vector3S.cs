using System;
using UnityEngine;

namespace UnityMultiplayer.Unity.DataTypes
{
  [Serializable]
  public struct Vector3S
  {
    public float X { get; set; }
    public float Y { get; set; }
    public float Z { get; set; }

    public Vector3S(float x, float y, float z)
    {
      X = x;
      Y = y;
      Z = z;
    }

    public static implicit operator Vector3(Vector3S vector3S) => new Vector3(vector3S.X, vector3S.Y, vector3S.Z);

    public static implicit operator Vector3S(Vector3 vector3) => new Vector3S(vector3.x, vector3.y, vector3.z);
  }
}
