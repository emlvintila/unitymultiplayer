using System.Collections;
using UnityMultiplayer.Unity.GameOperations;

namespace UnityMultiplayer.Unity.Net
{
  internal interface IPacketHandler
  {
    IEnumerator NetworkTickCoroutine();

    void SendPacket(GameUpdatePacket packet);
  }
}
