using System;
using UnityEngine;

namespace UnityMultiplayer.Unity.DataTypes
{
  [Serializable]
  public struct QuaternionS
  {
    public float X { get; set; }
    public float Y { get; set; }
    public float Z { get; set; }
    public float W { get; set; }

    public QuaternionS(float x, float y, float z, float w)
    {
      X = x;
      Y = y;
      Z = z;
      W = w;
    }

    public static implicit operator Quaternion(QuaternionS quaternionS) =>
      new Quaternion(quaternionS.X, quaternionS.Y, quaternionS.Z, quaternionS.W);

    public static implicit operator QuaternionS(Quaternion quaternion) =>
      new QuaternionS(quaternion.x, quaternion.y, quaternion.z, quaternion.w);
  }
}
