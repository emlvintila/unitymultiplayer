using UnityMultiplayer.Unity.GameOperations;

namespace UnityMultiplayer.Unity.Net
{
  public interface INetworkHandler
  {
    INetworkViewManager NetworkViewManager { get; }

    GameUpdatePacket GetInitializePacket();

    GameUpdatePacket GetNetworkTickPacket();

    void SendPacket(GameUpdatePacket packet);
  }
}
