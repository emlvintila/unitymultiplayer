using UnityEngine;

namespace UnityMultiplayer.Unity.DataTypes
{
  public struct PositionRotation
  {
    public Vector3 Position { get; set; }

    public Quaternion Rotation { get; set; }

    public PositionRotation(Vector3 position, Quaternion rotation)
    {
      Position = position;
      Rotation = rotation;
    }
  }
}
