using System.Collections.Generic;
using UnityMultiplayer.Unity.GameOperations;

namespace UnityMultiplayer.Unity.Net
{
  public interface INetworkDispatcher
  {
    void Dispatch(AbstractGameOperation operation);

    void DispatchAll(IEnumerable<AbstractGameOperation> gameOperations);
  }
}
