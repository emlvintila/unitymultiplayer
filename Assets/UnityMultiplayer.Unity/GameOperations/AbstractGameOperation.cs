using System;

namespace UnityMultiplayer.Unity.GameOperations
{
  [Serializable]
  public abstract class AbstractGameOperation
  {
    protected AbstractGameOperation(int viewId) => ViewId = viewId;

    public int ViewId { get; }
  }
}
