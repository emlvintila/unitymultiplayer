using System;
using UnityEngine;
using UnityMultiplayer.Unity.GameOperations;

namespace UnityMultiplayer.Unity.Net.NetworkBehaviours
{
  public abstract class NetworkBehaviour : MonoBehaviour, IGameOperationHandler
  {
    internal int InternalInstanceId;
    internal NetworkView InternalOwningView;

    public int InstanceId => InternalInstanceId;
    public NetworkView OwningView => InternalOwningView;

    public void HandleOperation(GameOperation operation)
    {
      if (operation.InstanceId != InstanceId)
        throw new ArgumentException(nameof(operation));

      // If IsAuthoritative is true, we handle the state ourselves
      if (!OwningView.IsAuthoritative)
        DoHandleOperation(operation);
    }

    public abstract GameOperation GenerateNetworkOperation();

    protected virtual void DoHandleOperation(GameOperation operation) { }

    public static explicit operator NetworkView(NetworkBehaviour networkBehaviour) => networkBehaviour.OwningView;
  }
}
