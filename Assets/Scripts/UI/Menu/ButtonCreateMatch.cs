using UnityEngine;
using UnityEngine.UI;
using UnityMultiplayer.Unity.Net;

namespace UI.Menu
{
  [RequireComponent(typeof(Button))]
  public class ButtonCreateMatch : MonoBehaviour
  {
    #if !UNITY_SERVER
    private Button button;

    private void Awake()
    {
      if (!button)
        button = GetComponent<Button>();
      button.onClick.AddListener(ClickCallback);
      NetworkManager.Instance.Client.GameClient.MatchRoomCreated += Debug.Log;
    }

    private void ClickCallback()
    {
      NetworkManager.Instance.Client.GameClient.CreateMatchRoom();
    }

    private void Reset()
    {
      button = GetComponent<Button>();
    }
    #endif
  }
}
