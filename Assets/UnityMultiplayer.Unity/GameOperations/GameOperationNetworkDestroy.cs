using System;

namespace UnityMultiplayer.Unity.GameOperations
{
  [Serializable]
  public class GameOperationNetworkDestroy : AbstractGameOperation
  {
    public GameOperationNetworkDestroy(int viewId) : base(viewId) { }
  }
}
