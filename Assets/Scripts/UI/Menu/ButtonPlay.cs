using UnityEngine;
using UnityEngine.UI;
using UnityMultiplayer.Unity.Net;

namespace UI.Menu
{
  [RequireComponent(typeof(Button))]
  public class ButtonPlay : MonoBehaviour
  {
    #if !UNITY_SERVER
    [SerializeField, HideInInspector]
    private Button button;

    [SerializeField]
    private InputField globalServerInputField;

    [SerializeField]
    private SubMenu playMenu;

    private void Awake()
    {
      if (!button)
        button = GetComponent<Button>();
      button.onClick.AddListener(ClickCallback);
    }

    private async void ClickCallback()
    {
      button.interactable = false;
      await NetworkManager.Instance.Client.GameClient
        .ConnectToGlobal(globalServerInputField.text)
        .ConfigureAwait(true);
      if (NetworkManager.Instance.Client.GameClient.IsConnected)
        playMenu.Show();
      else
        Debug.Log("Failed to connect to the global server.");
      button.interactable = true;
    }

    private void Reset()
    {
      button = GetComponent<Button>();
    }
    #endif
  }
}
