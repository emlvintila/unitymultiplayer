using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using UnityMultiplayer.Shared.Messaging;

namespace UnityMultiplayer.Unity.GameOperations
{
  [Serializable]
  public class GameUpdatePacket
  {
    #if UNITY_SERVER
    private const MessageType VALID_RECEIVED_MESSAGE_TYPE = MessageType.ClientGameUpdate;
    #else
    private const MessageType VALID_RECEIVED_MESSAGE_TYPE = MessageType.ServerGameUpdate;
    #endif
    public List<AbstractGameOperation> GameOperations { get; }

    public GameUpdatePacket() : this(Enumerable.Empty<AbstractGameOperation>()) { }

    public GameUpdatePacket(IEnumerable<AbstractGameOperation> operations) =>
      GameOperations = new List<AbstractGameOperation>(operations);

    public Message AsMessage()
    {
      using (MemoryStream memoryStream = new MemoryStream())
      {
        BinaryFormatter binaryFormatter = new BinaryFormatter();
        binaryFormatter.Serialize(memoryStream, this);
        byte[] data = memoryStream.ToArray();
        #if UNITY_SERVER
        return MessageEncoder.ServerGameUpdate(data);
        #else
        return MessageEncoder.ClientGameUpdate(data);
        #endif
      }
    }

    public static GameUpdatePacket FromMessage(Message message)
    {
      if (message.MessageType != VALID_RECEIVED_MESSAGE_TYPE)
        throw new ArgumentException(nameof(message));

      using (MemoryStream memoryStream = new MemoryStream(message.Data))
      {
        BinaryFormatter binaryFormatter = new BinaryFormatter();
        return (GameUpdatePacket) binaryFormatter.Deserialize(memoryStream);
      }
    }
  }
}
