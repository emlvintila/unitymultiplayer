using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneManagerEx : MonoBehaviour
{
  public static SceneManagerEx Instance { get; protected set; }

  public string ActiveSceneName => SceneManager.GetActiveScene().name;

  private void Awake()
  {
    if (Instance && Instance != this)
    {
      Destroy(this);
      return;
    }

    Instance = this;
  }
}
