using System;

namespace UnityMultiplayer.Unity.GameOperations
{
  [Serializable]
  public class GameOperation : AbstractGameOperation
  {
    public GameOperation(int viewId, int instanceId) : base(viewId) => InstanceId = instanceId;

    public int InstanceId { get; }
  }
}
