using UnityMultiplayer.Unity.GameOperations;

namespace UnityMultiplayer.Unity.Net.NetworkBehaviours
{
  public interface IGameOperationHandler
  {
    void HandleOperation(GameOperation operation);

    GameOperation GenerateNetworkOperation();
  }
}
