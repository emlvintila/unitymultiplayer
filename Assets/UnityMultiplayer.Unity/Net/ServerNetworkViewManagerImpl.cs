using System;
using System.Collections.Generic;
using UnityMultiplayer.Unity.DataTypes;
using UnityMultiplayer.Unity.Net.NetworkBehaviours;

namespace UnityMultiplayer.Unity.Net
{
  public sealed class ServerNetworkViewManagerImpl : AbstractNetworkViewManagerImpl
  {
    private IGenerator<int> InstanceIdGenerator { get; } = new IntGeneratorImpl();

    protected override void RegisterBehavioursForView(NetworkView view, IEnumerable<NetworkBehaviour> behaviours,
      IEnumerable<int> instanceIds)
    {
      if (view is null)
        throw new ArgumentNullException(nameof(view));
      if (behaviours is null)
        throw new ArgumentNullException(nameof(behaviours));

      foreach (NetworkBehaviour behaviour in behaviours)
      {
        behaviour.InternalInstanceId = InstanceIdGenerator.Next();
        view.Register(behaviour);
      }
    }
  }
}
