using System;
using UnityEngine;
using UnityMultiplayer.Unity.GameOperations;

namespace UnityMultiplayer.Unity.Net.NetworkBehaviours
{
  [RequireComponent(typeof(Transform))]
  public sealed class NetworkTransform : NetworkBehaviour
  {
    private Transform selfTransform;

    private void Awake()
    {
      selfTransform = GetComponent<Transform>();
    }

    private void Reset()
    {
      selfTransform = GetComponent<Transform>();
    }

    protected override void DoHandleOperation(GameOperation operation)
    {
      if (!(operation is GameOperationUpdateTransform updateTransform))
        throw new ArgumentException(nameof(operation));

      selfTransform.position = updateTransform.Position;
      selfTransform.rotation = updateTransform.Rotation;
      selfTransform.localScale = updateTransform.Scale;
    }

    public override GameOperation GenerateNetworkOperation() =>
      new GameOperationUpdateTransform(OwningView.ViewId, InstanceId, selfTransform.position, selfTransform.rotation,
        selfTransform.localScale);
  }
}
