﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityMultiplayer.Shared;
using UnityMultiplayer.Shared.Messaging;
using UnityMultiplayer.Unity.DataTypes;
using UnityMultiplayer.Unity.GameOperations;
using UnityMultiplayer.Unity.Net.NetworkBehaviours;

namespace UnityMultiplayer.Unity.Net
{
  public sealed class MatchServerBehaviour : MonoBehaviour, INetworkHandler
  {
    private const string PORT_ARG_NAME = "--port=";
    private const int SPAWN_RADIUS = 5;

    private readonly List<AbstractGameOperation> clientOperations = new List<AbstractGameOperation>();
    private int networkBehaviourInstanceIdCounter;
    private int networkViewIdCounter;

    public static MatchServerBehaviour Instance { get; private set; }

    internal Server MatchServer { get; private set; }

    private IGenerator<int> ViewIdGenerator { get; } = new IntGeneratorImpl();

    private IGenerator<PositionRotation> SpawnPositionGenerator { get; } = new PositionRotationGeneratorImpl();

    public GameUpdatePacket GetInitializePacket()
    {
      GameUpdatePacket packet = new GameUpdatePacket();
      NetworkBehaviourPlaceholder[] placeholders = FindObjectsOfType<NetworkBehaviourPlaceholder>();
      foreach (NetworkBehaviourPlaceholder placeholder in placeholders)
      {
        Transform placeholderTransform = placeholder.transform;
        GameOperationNetworkInstantiate instantiate = NetworkViewManager.NetworkInstantiate(placeholder.PrefabName,
          placeholderTransform.position,
          placeholderTransform.rotation, NetworkManager.SERVER_PLAYER_ID, ViewIdGenerator.Next(), null);
        NetworkView view = NetworkViewManager.NetworkViewsMap[instantiate.ViewId];
        Transform viewTransform = view.transform;
        GameOperationUpdateTransform updateTransform = new GameOperationUpdateTransform(view.ViewId,
          view.NetworkTransform.InstanceId,
          viewTransform.position, viewTransform.rotation, viewTransform.localScale);
        packet.GameOperations.Add(instantiate);
        packet.GameOperations.Add(updateTransform);
        Destroy(placeholder);
      }

      foreach (AuthenticatedUser user in MatchServer.AuthenticatedUsers.Values)
      {
        PositionRotation spawn = SpawnPositionGenerator.Next();
        GameOperationNetworkInstantiate instantiate = NetworkViewManager.NetworkInstantiate(
          NetworkManager.PLAYER_PREFAB_NAME, spawn.Position,
          spawn.Rotation, user.Id, ViewIdGenerator.Next(), null);
        NetworkView view = NetworkViewManager.NetworkViewsMap[instantiate.ViewId];
        Transform viewTransform = view.transform;
        GameOperationUpdateTransform updateTransform = new GameOperationUpdateTransform(view.ViewId,
          view.NetworkTransform.InstanceId,
          viewTransform.position, viewTransform.rotation, viewTransform.localScale);
        packet.GameOperations.Add(instantiate);
        packet.GameOperations.Add(updateTransform);
      }

      return packet;
    }

    public INetworkViewManager NetworkViewManager { get; private set; }

    public GameUpdatePacket GetNetworkTickPacket()
    {
      DispatchAndClear();
      IEnumerable<GameOperation> gameOperations = NetworkViewManager.NetworkViewsMap.Values
//        .Where(view => view.IsAuthoritative)
        .Aggregate(Enumerable.Empty<NetworkBehaviour>(),
          (enumerable, view) => enumerable.Concat(view.NetworkBehavioursMap.Values))
        .Select(behaviour => behaviour.GenerateNetworkOperation())
        .Where(operation => operation != null);
      return new GameUpdatePacket(gameOperations);
    }

    public void SendPacket(GameUpdatePacket packet)
    {
      Message message = packet.AsMessage();
      foreach (AuthenticatedUser user in MatchServer.AuthenticatedUsers.Values)
        user.MessageHandler.SendMessage(message);
    }

    private void FixedUpdate()
    {
      if (SceneManager.GetActiveScene().name != NetworkManager.Instance.MainScene)
        return;

      DispatchAndClear();
    }

    private void DispatchAndClear()
    {
      NetworkViewManager.DispatchAll(clientOperations);
      clientOperations.Clear();
    }

    private async void Awake()
    {
      if (Instance && Instance != this)
      {
        Destroy(this);
        return;
      }

      Instance = this;
      DontDestroyOnLoad(this);
      string[] args = Environment.GetCommandLineArgs();
      int port = int.Parse(args.First(arg => arg.StartsWith(PORT_ARG_NAME)).Substring(PORT_ARG_NAME.Length));
      MatchServer = new Server {ListenPort = port};
      await MatchServer.StartAsync();
      MatchServer.ClientConnected += MatchServerOnClientConnected;
      NetworkViewManager = new ServerNetworkViewManagerImpl();
      ((IInitializableGenerator<PositionRotation, float>) SpawnPositionGenerator).Initialize(SPAWN_RADIUS);
    }

    private void MatchServerOnClientConnected(AuthenticatedUser user)
    {
      user.MessageHandler.SendMessage(MessageEncoder.InitNetworkManager(user.Id));
      user.MessageHandler.MessageReceived += UserOnMessageReceived;
      // TODO: Find another way to start the match
      if (MatchServer.AuthenticatedUsers.Count == 1)
      {
        foreach (AuthenticatedUser u in MatchServer.AuthenticatedUsers.Values)
          u.MessageHandler.SendMessage(MessageEncoder.LoadScene(NetworkManager.Instance.MainScene));
        SceneManager.LoadSceneAsync(NetworkManager.Instance.MainScene);
      }
    }

    private void UserOnMessageReceived(AuthenticatedUser user, Message message)
    {
      // ReSharper disable once SwitchStatementMissingSomeCases
      switch (message.MessageType)
      {
        case MessageType.ClientGameUpdate:
          GameUpdatePacket packet = GameUpdatePacket.FromMessage(message);
          IEnumerable<AbstractGameOperation> validOperations = packet.GameOperations.Where(operation =>
            NetworkViewManager.NetworkViewsMap.ContainsKey(operation.ViewId) &&
            NetworkViewManager.NetworkViewsMap[operation.ViewId].PlayerId == user.Id);
          clientOperations.AddRange(validOperations);
          break;
      }
    }
  }
}
