namespace UnityMultiplayer.Unity.DataTypes
{
  public interface IInitializableGenerator<out TOut, in TInitialize> : IGenerator<TOut>
  {
    bool Initialized { get; }

    void Initialize(TInitialize value);
  }
}
