using System.Collections.Generic;
using UnityEngine;
using UnityMultiplayer.Unity.Net.NetworkBehaviours;

namespace UnityMultiplayer.Unity.Net
{
  [RequireComponent(typeof(NetworkTransform))]
  public class NetworkView : MonoBehaviour
  {
    protected readonly Dictionary<int, NetworkBehaviour> NetworkBehaviours = new Dictionary<int, NetworkBehaviour>();
    internal int InternalPlayerId;
    internal int InternalViewId;

    public bool IsAuthoritative => NetworkManager.Instance.PlayerId == PlayerId;
    public int ViewId => InternalViewId;
    public int PlayerId => InternalPlayerId;
    public IReadOnlyDictionary<int, NetworkBehaviour> NetworkBehavioursMap => NetworkBehaviours;
    public NetworkTransform NetworkTransform { get; private set; }

    private void Awake()
    {
      NetworkTransform = GetComponent<NetworkTransform>();
    }

    public void Register(NetworkBehaviour networkBehaviour)
    {
      if (NetworkBehaviours.ContainsKey(networkBehaviour.InstanceId))
        return;

      NetworkBehaviours[networkBehaviour.InstanceId] = networkBehaviour;
      networkBehaviour.InternalOwningView = this;
    }
  }
}
