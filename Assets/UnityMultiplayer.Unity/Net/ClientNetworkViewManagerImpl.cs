using System;
using System.Collections.Generic;
using System.Linq;
using UnityMultiplayer.Unity.Net.NetworkBehaviours;

namespace UnityMultiplayer.Unity.Net
{
  public class ClientNetworkViewManagerImpl : AbstractNetworkViewManagerImpl
  {
    protected override void RegisterBehavioursForView(NetworkView view, IEnumerable<NetworkBehaviour> behaviours,
      IEnumerable<int> instanceIds)
    {
      if (view is null)
        throw new ArgumentNullException(nameof(view));
      if (behaviours is null)
        throw new ArgumentNullException(nameof(behaviours));
      if (instanceIds is null)
        throw new ArgumentNullException(nameof(instanceIds));

      NetworkBehaviour[] behavioursArray = behaviours as NetworkBehaviour[] ?? behaviours.ToArray();
      int[] idsArray = instanceIds as int[] ?? instanceIds.ToArray();
      if (behavioursArray.Length != idsArray.Length)
        throw new ArgumentException($"{nameof(behaviours)}.Count != {nameof(instanceIds)}.Count");

      for (int i = 0; i < behavioursArray.Length; ++i)
      {
        NetworkBehaviour behaviour = behavioursArray[i];
        int id = idsArray[i];
        behaviour.InternalInstanceId = id;
        view.Register(behaviour);
      }
    }
  }
}
