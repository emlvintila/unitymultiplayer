using System.Reflection;
using log4net;
using log4net.Appender;
using log4net.Config;
using log4net.Layout;
using UnityEngine;

public class Init : MonoBehaviour
{
  private void Awake()
  {
    Assembly assembly = Assembly.GetExecutingAssembly();
    BasicConfigurator.Configure(LogManager.GetRepository(assembly),
      new DebugAppender
      {
        Layout = new PatternLayout("%-5level %date{HH:mm:ss} %-20logger{1}%message%newline%exception")
      });
    ILog log = LogManager.GetLogger(GetType());
    log.Debug($"{nameof(Init)}.{nameof(Awake)}");
  }
}
