using System;

namespace UnityMultiplayer.Unity.GameOperations
{
  [Serializable]
  public abstract class GameOperationUpdateComponent : GameOperation
  {
    protected GameOperationUpdateComponent(int viewId, int instanceId) : base(viewId, instanceId) { }
  }
}
