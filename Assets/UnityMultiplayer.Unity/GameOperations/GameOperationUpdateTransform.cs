using System;
using UnityMultiplayer.Unity.DataTypes;

namespace UnityMultiplayer.Unity.GameOperations
{
  [Serializable]
  public class GameOperationUpdateTransform : GameOperationUpdateComponent
  {
    public GameOperationUpdateTransform(int viewId, int instanceId, Vector3S position, QuaternionS rotation,
      Vector3S scale) :
      base(viewId, instanceId)
    {
      Position = position;
      Rotation = rotation;
      Scale = scale;
    }

    public Vector3S Position { get; }
    public QuaternionS Rotation { get; }
    public Vector3S Scale { get; }
  }
}
