﻿using System;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityMultiplayer.Shared.Messaging;
using UnityMultiplayer.Unity.GameOperations;
using UnityMultiplayer.Unity.Net.NetworkBehaviours;

namespace UnityMultiplayer.Unity.Net
{
  public sealed class GameClientBehaviour : MonoBehaviour, INetworkHandler
  {
    private readonly List<AbstractGameOperation> serverOperations = new List<AbstractGameOperation>();
    private AsyncOperation loadSceneOperation;

    private static GameClientBehaviour Instance { get; set; }

    public GameClient GameClient { get; private set; }

    public bool LoadingScene => loadSceneOperation != null && !loadSceneOperation.isDone;

    public INetworkViewManager NetworkViewManager { get; private set; }

    public GameUpdatePacket GetInitializePacket() => null;

    public GameUpdatePacket GetNetworkTickPacket()
    {
      DispatchAndClear();
      IEnumerable<GameOperation> operations = NetworkViewManager.NetworkViewsMap.Values
        .Where(view => view.IsAuthoritative)
        .Select(view => view.NetworkBehavioursMap.Values)
        .Aggregate(Enumerable.Empty<NetworkBehaviour>(), (enumerable, current) => enumerable.Concat(current))
        .Select(behaviour => behaviour.GenerateNetworkOperation())
        .Where(operation => operation != null);

      return new GameUpdatePacket(operations);
    }

    public void SendPacket(GameUpdatePacket packet)
    {
      GameClient.MessageHandler.SendMessage(packet.AsMessage());
    }

    private void Awake()
    {
      if (Instance && Instance != this)
      {
        Destroy(this);
        return;
      }

      Instance = this;
      DontDestroyOnLoad(this);
      GameClient = new GameClient(GameClientOnMessageReceived);
      NetworkViewManager = new ClientNetworkViewManagerImpl();
    }

    private void FixedUpdate()
    {
      if (LoadingScene || SceneManager.GetActiveScene().name != NetworkManager.Instance.MainScene)
        return;

      DispatchAndClear();
    }

    private void DispatchAndClear()
    {
      NetworkViewManager.DispatchAll(serverOperations);
      serverOperations.Clear();
    }

    private void GameClientOnMessageReceived(GameClient self, Message message)
    {
      // ReSharper disable once SwitchStatementMissingSomeCases
      switch (message.MessageType)
      {
        case MessageType.InitNetworkManager:
          int playerId = MessageDecoder.InitNetworkManager(message);
          NetworkManager.Instance.Init(playerId);
          break;
        case MessageType.LoadScene:
          if (self.State != GameClient.GameClientState.ConnectedToMatchServer)
            throw new InvalidOperationException();

          NetworkManager.Instance.RunOnMainThread(() =>
          {
            loadSceneOperation = SceneManager.LoadSceneAsync(MessageDecoder.LoadScene(message));
          });
          break;
        case MessageType.ServerGameUpdate:
          GameUpdatePacket packet = GameUpdatePacket.FromMessage(message);
          serverOperations.AddRange(packet.GameOperations);
          break;
      }
    }
  }
}
