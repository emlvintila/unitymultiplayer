using System;
using System.Collections.Generic;
using System.Linq;

namespace UnityMultiplayer.Unity.GameOperations
{
  [Serializable]
  public class GameOperationNetworkInstantiate : AbstractGameOperation
  {
    public GameOperationNetworkInstantiate(int viewId, int playerId, string prefabName,
      IEnumerable<int> networkBehavioursInstanceIds) : base(viewId)
    {
      PlayerId = playerId;
      PrefabName = prefabName;
      InstanceIds = networkBehavioursInstanceIds.ToArray();
    }

    public int PlayerId { get; }
    public string PrefabName { get; }

    public IEnumerable<int> InstanceIds { get; }
  }
}
