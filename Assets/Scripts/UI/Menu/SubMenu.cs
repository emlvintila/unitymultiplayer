namespace UI.Menu
{
  public class SubMenu : MenuBehaviour
  {
    #if !UNITY_SERVER
    public MenuBehaviour Owner { get; set; }

    public void Show()
    {
      Owner.HideAll();
      gameObject.SetActive(true);
    }

    public void Hide()
    {
      gameObject.SetActive(false);
    }
    #endif
  }
}
