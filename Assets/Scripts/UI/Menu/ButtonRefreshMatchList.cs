using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityMultiplayer.Shared;
using UnityMultiplayer.Shared.Messaging;
using UnityMultiplayer.Unity;
using UnityMultiplayer.Unity.Net;

namespace UI.Menu
{
  [RequireComponent(typeof(Button))]
  public class ButtonRefreshMatchList : MonoBehaviour
  {
    #if !UNITY_SERVER
    [SerializeField]
    private GameObject container;

    [SerializeField]
    private GameObject prefab;

    private void OnEnable()
    {
      NetworkManager.Instance.Client.GameClient.MessageHandler.MessageReceived += GameClientOnMessageReceived;
    }

    private void GameClientOnMessageReceived(GameClient client, Message message)
    {
      // ReSharper disable once SwitchStatementMissingSomeCases
      switch (message.MessageType)
      {
        case MessageType.PopulateMatchList:
          Clear();
          IEnumerable<MatchInfo> list = MessageDecoder.PopulateMatchList(message);
          foreach (MatchInfo matchInfo in list)
          {
            GameObject clone = Instantiate(prefab, container.transform);
            clone.GetComponent<MatchInfoBehaviour>().MatchInfo = matchInfo;
          }

          break;
      }
    }

    private void Clear()
    {
      foreach (Transform child in container.transform)
        Destroy(child.gameObject);
    }

    private void OnDisable()
    {
      NetworkManager.Instance.Client.GameClient.MessageHandler.MessageReceived -= GameClientOnMessageReceived;
    }
    #endif
  }
}
