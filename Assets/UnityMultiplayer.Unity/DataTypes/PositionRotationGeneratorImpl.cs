using System;
using UnityEngine;
using Random = UnityEngine.Random;

namespace UnityMultiplayer.Unity.DataTypes
{
  public sealed class PositionRotationGeneratorImpl : IInitializableGenerator<PositionRotation, float>
  {
    private float _radius;

    public PositionRotation Next()
    {
      if (!Initialized)
        throw new InvalidOperationException();

      Vector3 position = Random.onUnitSphere * _radius;
      position.y = 0;
      Quaternion rotation = Quaternion.identity;
      return new PositionRotation(position, rotation);
    }

    public bool Initialized { get; private set; }

    public void Initialize(float radius)
    {
      Initialized = true;
      _radius = radius;
    }
  }
}
