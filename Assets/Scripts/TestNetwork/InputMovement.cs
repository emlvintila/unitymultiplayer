using System.Collections;
using log4net;
using UnityEngine;
using UnityMultiplayer.Unity.GameOperations;
using UnityMultiplayer.Unity.Net.NetworkBehaviours;

namespace TestNetwork
{
  [RequireComponent(typeof(NetworkTransform))]
  public class InputMovement : NetworkBehaviour
  {
    private ILog log;
    private Transform selfTransform;

    private void Awake()
    {
      selfTransform = transform;
      log = LogManager.GetLogger(GetType());
      StartCoroutine(PrintSelfTransform());
    }

    private IEnumerator PrintSelfTransform()
    {
      while (true)
      {
        log.Debug($"{nameof(InputMovement)}.transform.position={selfTransform.position}");
        yield return new WaitForSecondsRealtime(1);
      }
    }

    private void FixedUpdate()
    {
      if (!OwningView.IsAuthoritative)
        return;

      Vector3 horizontal = selfTransform.right * Input.GetAxis("Horizontal");
      Vector3 vertical = selfTransform.forward * Input.GetAxis("Vertical");
      Vector3 delta = (horizontal + vertical) * Time.deltaTime;
      selfTransform.position += delta;
    }

    public override GameOperation GenerateNetworkOperation() => null;
  }
}
