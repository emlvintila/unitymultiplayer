namespace UnityMultiplayer.Unity.DataTypes
{
  public sealed class IntGeneratorImpl : IGenerator<int>
  {
    private int current;

    public int Next() => ++current;
  }
}
